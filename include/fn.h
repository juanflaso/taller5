typedef struct {
                char *nombre;
                float precio;
                char *sku;
                int stock;
                float precioIVA;
}producto;

void bytesInt(int valor);
void bytesLong(long valor);
void bytesFloat(float valor);
void bytesDouble(double valor);
void showAddress(int *arr,int num, int len);
void direcciones(producto **arr, int *arraint);
void iva(producto *prod);
void datosURL(char *url);
void imc(float peso, float altura, float **resultado);
