#include <stdio.h>

void bytesInt(int valor) {
    unsigned char* bytes = &valor;
    for (int i = 0; i < sizeof(int); i++) {
        printf("Byte %d: %02X\n", i, bytes[i]);
    }
}

void bytesLong(long valor) {
    unsigned char* bytes = &valor;
    for (int i = 0; i < sizeof(long); i++) {
        printf("Byte %d: %02X\n", i, bytes[i]);
    }
}

void bytesFloat(float valor) {
    unsigned char* bytes = &valor;
    for (int i = 0; i < sizeof(float); i++) {
        printf("Byte %d: %02X\n", i, bytes[i]);
    }
}

void bytesDouble(double valor) {
    unsigned char* bytes = &valor;
    for (int i = 0; i < sizeof(double); i++) {
        printf("Byte %d: %02X\n", i, bytes[i]);
    }
}