#include <stdio.h>
#include <stdlib.h>

#define MAX_PRODUCTOS 5
#define MAX_ARRAY 3
/*
//Se definen las funciones a utilizarse en el ejercicio 5 y 6
void direcciones(producto **arr, int *arraint);
void iva(producto *prod);

int main(){
        producto *array[MAX_PRODUCTOS];
		
		//Creacion de varios punteros a un tipo de dato producto
		producto *p1=malloc(sizeof(producto));
		producto *p2=malloc(sizeof(producto));
		producto *p3=malloc(sizeof(producto));
		producto *p4=malloc(sizeof(producto));
		producto *p5 =malloc(sizeof(producto));
		printf("%p\n",p1);
		array[0]=p1;
		array[1]=p2;
		array[2]=p3;
		array[3]=p4;
		array[4]=p5;
		
		iva(p1);  //Aqui se esta realizando el ejercicio 5 del taller
		 printf("%.2f  \n",p1->precioIVA);
		
		int arrint[MAX_ARRAY]={0,2,4};
		direcciones(&array, &arrint);
		
		free(p1);
		free(p2);
		free(p3);
		free(p4);
		free(p5);

}*/
//Se define la estructura con nombre producto
typedef struct {
                char *nombre;
                float precio;
                char *sku;
                int stock;
                float precioIVA;
}producto;



//Implementacion del ejercicio 6
void direcciones(producto **arr , int *arrint){
	for(int i=0 ; i<MAX_ARRAY ; i++){
		producto *puntero= arr[arrint[i]];
		printf("Direccion %p\n", puntero);
	}
}

//Implementacion del ejercicio 5
void iva(producto *prod){
	char inpt[50];
    printf("Ingrese el precio del producto: \n");
    fgets(inpt, 50, stdin);//Se pide el precio para calcular el IVA de ese producto
	
	int precio=atoi(inpt);
	prod->precio=precio;
		
	float preciop=prod->precio*0.12;
		
    prod->precioIVA=prod->precio+preciop;//Se le asigna el precio del IVA al producto

}
