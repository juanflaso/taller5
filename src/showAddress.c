/*Esta funcion recibe un arreglo de enteros y un numero y muestra
la direccion de todos los elementos del numero solicitado.*/


void showAddress(int *arr,int num,int len){
	for (int i=0 ; i<len ; i++){
		if(*(arr+i)==num){
			printf("Direccion del elemento %x\n",&(*(arr+i)));
		}
	}
	return 0;
}
