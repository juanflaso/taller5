#include <stdio.h>
#include <stdlib.h>

void datosURL(char *url) {
    char *protocol = malloc(1);
    char *domain = malloc(1);
    char *type = malloc(4);
    char *country = malloc(3);
    char protocolFound = 0;
    char findingDomain = 0;
    int i = 0;

    //Busca el protocolo
    while (url[i]!= '\0' && protocolFound == 0) {
        if(url[i] == 58) { 
            //Si encuentra ':' quiere decir que lo encontró
            protocolFound = 1;
            findingDomain = 1;
        }  
        else {
            protocol[i] = url[i];
            protocol = realloc(protocol, i+2);
            i++;
        }
    }
    
    //Busca el dominio mientras la bandera findingDomain sea 1
    int domainSize = 1;
    while(url[i]!= '\0' && findingDomain == 1) {
        //Si encuentra el caracter '.' podría ser que encontró el dominio
        if(url[i] == 46) { 
            /*
            * Para que sea un dominio válido puede darse que en el índice i+4 haya:
            * '\' lo cual indica que la cadena ha terminado
            * '.' lo cual indica que se existe un tipo y después de este un código de país
            * '/' lo cual indica que se sigue con el path de la dirección
            */
            if(url[i+4] == '\0' || url[i+4] == 46 || url[i+4] == 47) {
                findingDomain = 0;
            }
            //Si no se encuentra se reutiliza la variable y se sigue buscando
            else {
                free(domain);
                domain = malloc(1); 
                domainSize = 1;
                i++;
            }
        }  
        else {
            //Se construye la cadena del dominio
            if(url[i] != 47 && url[i] != 58) {
                domain[domainSize-1] = url[i];
                domain = realloc(domain, ++domainSize);
            }
            i++;
        }
    }

    //se busca el tipo
    int typeSize = 0;
    while(url[++i]!= '\0' && typeSize < 3) {
        type[typeSize] = url[i];
        typeSize++;
    }
    
    //Se busca el código de país que podría no existir
    int countrySize = 0;
    while(url[i]!=47 && countrySize < 2 && url[i]!= '\0') {
        i++;
        country[countrySize] = url[i];
        countrySize++;
    }

    //comprueba que después del código de país exista '/' o '\0', de lo contrario no es una url válida
    if(url[i+1]!= 47 && url[i]+1 != '\0') {
        typeSize = 0;
    }
    
    if(typeSize == 3){
        printf("Protocolo: %s\n", protocol);
        printf("Dominio: %s\n", domain);
        printf("Tipo: %s\n", type);
        printf("Código país: ");
        if(countrySize == 2) {
            printf("%s", country);
        }
        printf("\n");
    }
    else {
        printf("Invalid input\n");
    }

    free(protocol);
    free(domain);
    free(type);
    free(country);
}