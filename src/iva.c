#include <stdio.h>
#include <stdlib.h>

typedef struct {
		char *nombre;
		float precio;
		char *sku;
		int stock;
		float precioIVA;
}producto;

void iva(producto *prod){
	char inpt[50];
    printf("Ingrese el precio del producto: \n");
    fgets(inpt, 50, stdin);
	int precio=atoi(inpt);
	prod->precio=precio;
	float preciop=prod->precio*0.12;
    prod->precioIVA=prod->precio+preciop;
}
