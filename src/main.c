#include <stdio.h>
#include "fn.h"

#define MAX 50
#define MAX_PRODUCTOS 5
#define MAX_ARRAY 3
int main(){
	char msj[MAX];
	int num=0;
	int len=0;
	//ejercicio 1
	printf("INGRESE UN NUMERO\n");
	fgets(msj,MAX,stdin);
	num=atoi(msj);
	printf("ENTERO\n");
	bytesInt(num);
	printf("LONG\n");
	bytesLong(num);
	printf("FLOAT\n");
	bytesFloat((float)num);
	printf("DOUBLE\n");
	bytesDouble((double)num);
	
	//ejercicio 2
	char *url="https://www.fiec.espol.edu.ec";
	datosURL("https://www.fiec.espol.edu.ec");
	
	//ejercicio 3
	int array[7]={12,18,25,11,59,12,12};
	printf("INGRESE UN NUMERO\n");
	fgets(msj,MAX,stdin);
	num=atoi(msj);
	len= sizeof(array)/sizeof(int);
	showAddress(&array,num,len);
	
	//ejercicio 4
	float *resultado;
	
	imc(80,1.70,&resultado);
	printf("TU IMC ES: %f\n",*resultado);
	
	producto *arrayp[MAX_PRODUCTOS];
		
		//Creacion de varios punteros a un tipo de dato producto
		producto *p1=malloc(sizeof(producto));
		producto *p2=malloc(sizeof(producto));
		producto *p3=malloc(sizeof(producto));
		producto *p4=malloc(sizeof(producto));
		producto *p5 =malloc(sizeof(producto));
		array[0]=p1;
		array[1]=p2;
		array[2]=p3;
		array[3]=p4;
		array[4]=p5;
		
		iva(p1);  //Aqui se esta realizando el ejercicio 5 del taller
		printf("%.2f  \n",p1->precioIVA);
		
		int arrint[MAX_ARRAY]={0,2,4};
		direcciones(&arrayp, &arrint);
		
		free(p1);
		free(p2);
		free(p3);
		free(p4);
		free(p5);
		
		
	return 0;
	
	
	
}